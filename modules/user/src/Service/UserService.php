<?php

namespace Sabre\user\Service;

use Sabre\Core\Service\Service;
use Sabre\user\User;
use Sabre\user\UserInterface;

/**
 * The User service.
 */
class UserService {

  /**
   * Cached users.
   *
   * @var array
   */
  private $users = [];

  /**
   * The Database service.
   *
   * @var \Sabre\Core\Service\DatabaseService
   */
  private $databaseService;

  /**
   * UserService constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->databaseService = Service::get('database');
  }

  /**
   * Logins user in this system.
   *
   * @param \Sabre\user\UserInterface $user
   *
   * @throws \Exception
   */
  public function login(UserInterface $user) {
    session_start();
    $this->databaseService->getContext()->table('session')->insert([
      'id' => session_id(),
      'user_id' => $user->getId(),
    ]);
  }

  /**
   * Logout current user.
   *
   * @throws \Exception
   */
  public function logout() {
    $this->databaseService->getContext()
      ->table('session')
      ->where('id', session_id())
      ->delete();

    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(), '', 0, '/');
  }

  /**
   * Is current User logged.
   *
   * @return bool
   *
   * @throws \Exception
   */
  public function isLogged(): bool {
    if (isset($_COOKIE[(session_name())])) {
      $data = $this->databaseService->getContext()->table('session')
        ->where('id', $_COOKIE[session_name()])
        ->fetch()
        ->toArray();

      return !empty($data);
    }

    return FALSE;
  }

  /**
   * Loads the User by ID.
   *
   * @param int $id
   *
   * @return \Sabre\user\UserInterface
   *
   * @throws \Exception
   */
  public function getUserById(int $id): UserInterface {
    $users = $this->getUsers();
    return $users[$id];
  }

  /**
   * Loads User by login (Telephone).
   *
   * @param string $telephone
   *
   * @return \Sabre\user\UserInterface
   *
   * @throws \Exception
   */
  public function getUserByTelephone(string $telephone): UserInterface {
    try {
      $data = $this->databaseService->getContext()->table('user')
        ->where('telephone', $telephone)
        ->fetch()
        ->toArray();

      $user = $this->getUserById($data['id']);
    }
    catch (\Error $error) {
      throw new \Exception('User with this Telephone does not exist.');
    }

    return $user;
  }

  /**
   * Gets User object list.
   *
   * @return \Sabre\user\UserInterface[]
   *
   * @throws \Exception
   */
  public function getUsers(): array {
    if (!$this->users) {
      $user_list = $this->databaseService->getContext()->table('user')->fetchAll();

      foreach ($user_list as $item) {
        $user = new User($item['name'], $item['telephone']);
        $user->setSurname($item['surname']);
        $user->setAddress($item['address']);
        $user->setPassword($item['password']);
        $user->setId($item['id']);

        $this->users[$user->getId()] = $user;
      }
    }

    if (!$this->users) {
      throw new \Exception('Users have not been added yet.');
    }

    return $this->users;
  }

  /**
   * Creates User's table in DB.
   *
   * @param \Sabre\user\UserInterface $user
   *
   * @throws \Exception
   */
  public function createUser(UserInterface $user): void {
    $database = $this->databaseService->getContext()->table('user');
    $data = $database->insert([
      'name' => $user->getName(),
      'surname' => $user->getSurname(),
      'telephone' => $user->getTelephone(),
      'address' => $user->getAddress(),
      'password' => $user->hashPassword(),
    ])->toArray();

    if ($data) {
      $user->setId($data['id']);
    }
    else {
      throw new \Exception('Failed to create user');
    }
  }

  /**
   * Updates User's data in DB.
   *
   * @param \Sabre\user\UserInterface $user
   *
   * @throws \Exception
   */
  public function updateUser(UserInterface $user): void {
    $password = $this->getOriginUserData($user)['password'];

    if ($user->getPassword() !== $password) {
      $password = $user->hashPassword();
    }

    $database = $this->databaseService->getContext()->table('user');
    $data = $database->where('id', $user->getId())->update([
      'name' => $user->getName(),
      'surname' => $user->getSurname(),
      'telephone' => $user->getTelephone(),
      'address' => $user->getAddress(),
      'password' => $password,
    ]);

    if (!$data) {
      throw new \Exception('Failed to save user.');
    }
  }

  /**
   * Delete User's data from DB.
   *
   * @param \Sabre\user\UserInterface $user
   *
   * @throws \Exception
   */
  public function deleteUser(UserInterface $user) {
    $this->databaseService->getContext()
      ->table('user')
      ->where('id', $user->getId())
      ->delete();
  }

  /**
   * Gets origin User's data from DB.
   *
   * @param \Sabre\user\UserInterface $user
   *
   * @return array
   *
   * @throws \Exception
   */
  public function getOriginUserData(UserInterface $user): array {
    $result = $this->databaseService->getContext()->table('user')
      ->where('id', $user->getId())
      ->fetch()
      ->toArray();

    if (!$result) {
      throw new \Exception('Failed load data.');
    }

    return $result;
  }

}
