<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Response;
use Sabre\Core\Service\Service;

/**
 * Controller for deleting user.
 */
class UserDeleteController implements ControllerInterface {

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * UserDeleteController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->userService = Service::get('user');
    $this->routeService = Service::get('route');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $current_route = $this->routeService->getCurrentRoute();
    $user_id = $current_route->getParameter('id');
    $user = $this->userService->getUserById($user_id);
    $user->delete();
    $this->userService->logout();

    Response::redirect($this->routeService->getRouteByName('front.page'));
  }

}
