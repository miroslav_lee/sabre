<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Renderer;
use Sabre\Core\Response;
use Sabre\Core\Service\Service;

/**
 * Controller for the Login page.
 */
class LoginController implements ControllerInterface {

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * LoginController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->userService = Service::get('user');
    $this->routeService = Service::get('route');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    if ($_SERVER["REQUEST_METHOD"] === 'POST') {
      $user = $this->userService->getUserByTelephone($_POST['login']);

      if ($user->isValidPassword($_POST['password'])) {
        $this->userService->login($user);
      }
      else {
        throw new \Exception('Password is not valid.');
      }

      // Redirect to User's profile after login.
      $route = $this->routeService->getRouteByName('user.view', [
        'id' => $user->getId(),
      ]);
      Response::redirect($route);
    }

    // Add Links to menu.
    $links[] = [
      'uri' => '/',
      'title' => 'Home',
    ];
    if ($this->userService->isLogged()) {
      $links[] = [
        'uri' => '/logout',
        'title' => 'Logout',
      ];
    }
    else {
      $links[] = [
        'uri' => '/login',
        'title' => 'Login',
      ];
      $links[] = [
        'uri' => '/register',
        'title' => 'Register',
      ];
    }
    $build['menu']['links'] = $links;

    $renderer = new Renderer('user__login', $build);
    return $renderer->render();
  }

}
