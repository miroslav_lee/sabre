<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Response;
use Sabre\Core\Service\Service;

/**
 * Controller for the Logout page.
 */
class LogoutController implements ControllerInterface {

  /**
   * The User Service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * LogoutController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->routeService = Service::get('route');
    $this->userService = Service::get('user');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $this->userService->logout();
    Response::redirect($this->routeService->getRouteByName('front.page'));
  }

}
