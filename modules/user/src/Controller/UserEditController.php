<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Renderer;
use Sabre\Core\Response;
use Sabre\Core\Service\Service;

/**
 * Controller for the User edit profile page.
 */
class UserEditController implements ControllerInterface {

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * UserEditController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->userService = Service::get('user');
    $this->routeService = Service::get('route');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $current_route = $this->routeService->getCurrentRoute();
    $user_id = $current_route->getParameter('id');
    $user = $this->userService->getUserById($user_id);

    if ($_SERVER["REQUEST_METHOD"] === 'POST') {
      $user->setName($_POST['name']);
      $user->setTelephone($_POST['telephone']);
      $user->setAddress($_POST['address']);
      $user->setSurname($_POST['surname']);

      if ($password = $_POST['password']) {
        $user->setPassword($_POST['password']);
      }

      $user->save();

      Response::redirect($current_route);
    }

    // Render the User's edit profile page.
    $build['user'] = $user;
    $build['action'] = $current_route->getUri();
    // Add Links to Menu.
    $links[] = [
      'uri' => '/',
      'title' => 'Home',
    ];
    $links[] = [
      'uri' => "/user/{$user->getId()}",
      'title' => 'Profile',
    ];
    if ($this->userService->isLogged()) {
      $links[] = [
        'uri' => '/logout',
        'title' => 'Logout',
      ];
    }
    else {
      $links[] = [
        'uri' => '/login',
        'title' => 'Login',
      ];
      $links[] = [
        'uri' => '/register',
        'title' => 'Register',
      ];
    }
    $build['menu']['links'] = $links;

    $renderer = new Renderer('user__edit', $build);
    return $renderer->render();
  }

}
