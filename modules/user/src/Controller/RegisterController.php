<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Renderer;
use Sabre\Core\Response;
use Sabre\Core\Service\Service;
use Sabre\user\User;

/**
 * Controller for the Register page.
 */
class RegisterController implements ControllerInterface {

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * The User Service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * RegisterController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->routeService = Service::get('route');
    $this->userService = Service::get('user');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    if ($_SERVER["REQUEST_METHOD"] === 'POST') {
      $data = $_POST;
      $user = new User($data['name'], $data['telephone']);
      $user->setPassword($data['password']);
      $user->setAddress($data['address']);
      $user->setSurname($data['surname']);
      $user->save();

      // Redirect to the Home page after registered.
      $route = $this->routeService->getRouteByName('front.page');
      Response::redirect($route);
    }

    // Add Links to Menu.
    $links[] = [
      'uri' => '/',
      'title' => 'Home',
    ];
    if ($this->userService->isLogged()) {
      $links[] = [
        'uri' => '/logout',
        'title' => 'Logout',
      ];
    }
    else {
      $links[] = [
        'uri' => '/login',
        'title' => 'Login',
      ];
    }
    $build['menu']['links'] = $links;

    $renderer = new Renderer('user__register', $build);
    return $renderer->render();
  }

}
