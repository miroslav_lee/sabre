<?php

namespace Sabre\user\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Renderer;
use Sabre\Core\Service\Service;

/**
 * Controller for the User's profile page.
 */
class UserController implements ControllerInterface {

  /**
   * The Route service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * UserController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->userService = Service::get('user');
    $this->routeService = Service::get('route');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $user_id = $this->routeService->getCurrentRoute()->getParameter('id');
    $user = $this->userService->getUserById($user_id);
    $build['user'] = $user;

    // Add Links to Menu.
    $links[] = [
      'uri' => '/',
      'title' => 'Home',
    ];
    if ($this->userService->isLogged()) {
      $links[] = [
        'uri' => '/logout',
        'title' => 'Logout',
      ];
    }
    else {
      $links[] = [
        'uri' => '/login',
        'title' => 'Login',
      ];
      $links[] = [
        'uri' => '/register',
        'title' => 'Register',
      ];
    }
    $build['menu']['links'] = $links;
    $renderer = new Renderer('user', $build);

    return $renderer->render();
  }

}
