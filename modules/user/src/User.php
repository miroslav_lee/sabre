<?php

namespace Sabre\user;

use Sabre\Core\Service\Service;

/**
 * User's object.
 */
class User implements UserInterface {

  /**
   * User's name.
   *
   * @var string
   */
  private $name;

  /**
   * User's telephone.
   *
   * @var string
   */
  private $telephone;

  /**
   * User's telephone.
   *
   * @var int
   */
  private $id;

  /**
   * User's password.
   *
   * @var string
   */
  private $password;

  /**
   * User's surname.
   *
   * @var string
   */
  private $surname;

  /**
   * User's address.
   *
   * @var string
   */
  private $address;

  /**
   * The Database service.
   *
   * @var \Sabre\Core\Service\DatabaseService
   */
  private $databaseService;

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * User constructor.
   *
   * @param string $name
   * @param string $telephone
   *
   * @throws \Exception
   */
  public function __construct(string $name, string $telephone) {
    $this->name = $name;
    $this->telephone = $telephone;
    $this->databaseService = Service::get('database');
    $this->userService = Service::get('user');
  }

  /**
   * {@inheritDoc}
   */
  public function setId($id): UserInterface {
    $this->id = $id;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setName(string $name): UserInterface {
    $this->name = $name;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setTelephone(string $telephone): UserInterface {
    $this->telephone = $telephone;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setSurname($surname): UserInterface {
    $this->surname = $surname;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setAddress($address): UserInterface {
    $this->address = $address;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setPassword(string $password): UserInterface {
    $this->password = $password;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritDoc}
   */
  public function getTelephone(): string {
    return $this->telephone;
  }

  /**
   * {@inheritDoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function getSurname(): string {
    return $this->surname;
  }

  /**
   * {@inheritDoc}
   */
  public function getAddress(): string {
    return $this->address;
  }

  /**
   * {@inheritDoc}
   */
  public function getPassword(): string {
    return $this->password;
  }

  /**
   * {@inheritDoc}
   */
  public function isValidPassword(string $password): bool {
    return password_verify($password, $this->getPassword());
  }

  /**
   * {@inheritDoc}
   */
  public function isNew(): bool {
    return !($this->getId());
  }

  /**
   * {@inheritDoc}
   */
  public function save(): UserInterface {
    if ($this->isNew()) {
      $this->userService->createUser($this);
    }
    else {
      $this->userService->updateUser($this);
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function delete() {
    $this->userService->deleteUser($this);

    foreach ($this as $key => $value) {
      unset($this->$key);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function hashPassword(): string {
    return password_hash($this->getPassword(), PASSWORD_BCRYPT);
  }

}
