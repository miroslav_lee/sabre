<?php

namespace Sabre\user;

interface UserInterface {

  /**
   * Sets User's ID.
   *
   * @param int $id
   *
   * @return \Sabre\user\UserInterface
   */
  public function setId(int $id): self;

  /**
   * Sets User's name.
   *
   * @param string $name
   *
   * @return $this
   */
  public function setName(string $name): self;

  /**
   * Sets User's telephone.
   *
   * @param string $telephone
   *
   * @return $this
   */
  public function setTelephone(string $telephone): self;

  /**
   * Sets surname.
   *
   * @param string $surname
   *
   * @return \Sabre\user\UserInterface
   */
  public function setSurname(string $surname): self;

  /**
   * Sets User's telephone.
   *
   * @param $address
   *
   * @return $this
   */
  public function setAddress(string $address): self;

  /**
   * Sets User's password.
   *
   * @param string $password
   *
   * @return $this
   */
  public function setPassword(string $password): self;

  /**
   * Gets User's name.
   *
   * @return string
   */
  public function getName(): string;

  /**
   * Gets User's telephone number.
   *
   * @return string
   */
  public function getTelephone(): string;

  /**
   * Gets User's ID.
   *
   * @return string|null
   */
  public function getId();

  /**
   * Gets User's surname.
   *
   * @return string
   */
  public function getSurname(): string;

  /**
   * Gets User's address.
   *
   * @return string
   */
  public function getAddress(): string;

  /**
   * Gets User's password.
   *
   * @return string
   */
  public function getPassword(): string;

  /**
   * Checks the validity of the password.
   *
   * @param string $password
   *
   * @return bool
   */
  public function isValidPassword(string $password): bool;

  /**
   * Is this User a new or exists in the DB.
   *
   * @return bool
   */
  public function isNew(): bool;

  /**
   * Writes User's data to DB.
   *
   * @return $this
   */
  public function save(): self;

  /**
   * Deletes this User from DB and destroy this object.
   */
  public function delete();

  /**
   * Hashes User's password.
   *
   * @return false|string|null
   */
  public function hashPassword(): string;

}
