<?php

namespace Sabre\home\Controller;

use Sabre\Core\Controller\ControllerInterface;
use Sabre\Core\Renderer;
use Sabre\Core\Service\Service;

/**
 * The Home page controller.
 */
class HomePageController implements ControllerInterface {

  /**
   * The User service.
   *
   * @var \Sabre\user\Service\UserService
   */
  private $userService;

  /**
   * HomePageController constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    $this->userService = Service::get('user');
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $links = [
      [
        'uri' => '/login',
        'title' => 'Login',
      ],
      [
        'uri' => '/register',
        'title' => 'Register',
      ],
    ];

    if ($this->userService->isLogged()) {
      $links = [
        [
          'uri' => '/logout',
          'title' => 'Logout',
        ],
      ];
    }
    $build['menu']['links'] = $links;

    try {
      $this->userService->getUsers();
      $build['users'] = $this->userService->getUsers();
    }
    catch (\Exception $exception) {
      $build['users'] = NULL;
    }

    $renderer = new Renderer('home', $build);
    return $renderer->render();
  }

}
