<?php

namespace Sabre\Core\Service;

use Nette\Caching\Storages\FileStorage;
use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Conventions\DiscoveredConventions;
use Nette\Database\Structure;
use Sabre\Core\Settings;

/**
 * The Database service.
 */
class DatabaseService {

  /**
   * Connection to Database.
   *
   * @var \Nette\Database\Connection
   */
  private $connection;

  /**
   * Gets Connection to database.
   *
   * @return \Nette\Database\Connection
   *
   * @throws \Exception
   */
  public function getConnection() {
    if (!$this->connection) {
      $database = Settings::get('database');
      $this->connection = new Connection("mysql:host={$database['host']};dbname={$database['name']}", $database['user'], $database['password']);
    }

    return $this->connection;
  }

  /**
   * Gets Context of database.
   *
   * @return \Nette\Database\Context
   *
   * @throws \Exception
   */
  public function getContext(): Context {
    $connection = $this->getConnection();
    $tmp_directory = Settings::get('tmp');
    $structure = new Structure($connection, new FileStorage($tmp_directory));
    $conventions = new DiscoveredConventions($structure);
    return new Context($connection, $structure, $conventions);
  }

}
