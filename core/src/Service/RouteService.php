<?php

namespace Sabre\Core\Service;

use Sabre\Core\Route;
use Sabre\Core\RouteInterface;

/**
 * The Route service.
 */
class RouteService {

  /**
   * Routes list.
   *
   * @var \Sabre\Core\RouteInterface[]
   */
  private $routes = [];

  /**
   * Get Route of current request.
   *
   * @return \Sabre\Core\Route
   *
   * @throws \Exception
   */
  public function getCurrentRoute(): RouteInterface {
    $request_path = parse_url($_SERVER['REQUEST_URI'])['path'];
    $current_path = $request_path;
    $routes = $this->getAllRoutes();

    foreach ($routes as $name => $route) {
      if ($arguments = $route->getArguments()) {
        $uri_components = explode('/', $current_path);
        $current_path = implode('/', array_replace($uri_components, $arguments));
      }

      if ($current_path === $route->getPath()) {
        $route->setUri($request_path);
        return $route;
      }
    }

    throw new \Exception('Route does not exist.');
  }

  /**
   * Gets Route by name.
   *
   * @param string $name
   * @param array $parameters
   *
   * @return \Sabre\Core\RouteInterface
   *
   * @throws \Exception
   */
  public function getRouteByName(string $name, array $parameters = []) {
    $routes = $this->getAllRoutes();
    $route = $routes[$name];
    $route->setParameters($parameters);

    return $route;
  }

  /**
   * Gets Routes list.
   *
   * @return \Sabre\Core\RouteInterface[]
   *
   * @throws \Exception
   */
  protected function getAllRoutes() {
    if (!$this->routes) {
      $results = [];
      $files = glob("./{core,modules\/*}/*.routing.yml", GLOB_BRACE);

      foreach ($files as $file) {
        $yaml_data = yaml_parse_file($file);

        if ($yaml_data) {
          foreach ($yaml_data as $route_name => $data) {
            $controller_class_name = $data['controller'];
            $controller = new $controller_class_name();

            $route = new Route($route_name, $data['path'], $controller);
            $route->setTitle($data['title']);

            $results[$route_name] = $route;
          }
        }
        else {
          throw new \Exception('Something wrong in some routing.yml file.');
        }
      }

      $this->routes = $results;
    }

    return $this->routes;
  }

}
