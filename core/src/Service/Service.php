<?php

namespace Sabre\Core\Service;

/**
 * Class for init and returns services.
 */
class Service {

  /**
   * Cached services.
   *
   * @var object[]
   */
  private static $instances = [];

  /**
   * Gets the Service by name.
   *
   * @param $name
   *
   * @return object
   *
   * @throws \Exception
   */
  public static function get($name) {
    if (!isset(self::$instances[$name])) {
      throw new \Exception('This service does not exist.');
    }

    return self::$instances[$name];
  }

  /**
   * Initialize all services object from yml files.
   *
   * @throws \Exception
   */
  public static function init() {
    $files = glob("./{core,modules\/*}/*.services.yml", GLOB_BRACE);

    foreach ($files as $file) {
      $yaml_data = yaml_parse_file($file);

      if ($yaml_data) {
        foreach ($yaml_data as $name => $data) {
          $class_name = $data['class'];
          self::$instances[$name] = new $class_name();
        }
      }
      else {
        throw new \Exception('Something wrong in some services.yml file.');
      }
    }
  }

}
