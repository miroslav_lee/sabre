<?php

namespace Sabre\Core;

/**
 * Provide access to Settings of this site.
 */
class Settings {

  /**
   * Gets the Settings parameter.
   *
   * @param string $settings
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public static function get(string $settings) {
    require './settings.php';

    switch ($settings) {
      case 'database':
        return $database;
      case 'tmp':
        return $tmp_directory;
      default:
        throw new \Exception('Error in the name of the setting');
    }
  }

}
