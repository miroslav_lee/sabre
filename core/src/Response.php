<?php

namespace Sabre\Core;

class Response {

  /**
   * Redirects to another page.
   *
   * @param \Sabre\Core\RouteInterface $route
   * @param null $status_code
   */
  public static function redirect(RouteInterface $route, $status_code = NULL) {
    header('Location: ' . $route->getUri(), TRUE, $status_code);
  }

}
