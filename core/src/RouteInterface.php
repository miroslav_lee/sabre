<?php

namespace Sabre\Core;

use Sabre\Core\Controller\ControllerInterface;

interface RouteInterface {

  /**
   * Sets title.
   *
   * @param string $title
   *
   * @return $this
   */
  public function setTitle(string $title): self;

  /**
   * Sets route parameters.
   *
   * @param array $parameters
   *
   * @return $this
   */
  public function setParameters(array $parameters): self;

  /**
   * Sets clear URI.
   *
   * @param string $uri
   *
   * @return $this
   */
  public function setUri(string $uri): self;

  /**
   * Gets clear URI.
   *
   * @return string
   */
  public function getUri(): string;

  /**
   * Gets Route's name.
   *
   * @return string
   */
  public function getName(): string;

  /**
   * Gets Route's path (from routing.yml).
   *
   * @return string
   */
  public function getPath(): string;

  /**
   * Gets Page title.
   *
   * @return string
   */
  public function getTitle(): string;

  /**
   * Gets the Controller object.
   *
   * @return \Sabre\Core\Controller\ControllerInterface
   */
  public function getController(): ControllerInterface;

  /**
   * Gets array with path's components.
   *
   * @return array
   */
  public function getPathComponents(): array;

  /**
   * Gets argument of path.
   *
   * @return array
   */
  public function getArguments(): array;

  /**
   * Gets url parameter.
   *
   * @param string $name
   *
   * @return mixed
   */
  public function getParameter(string $name);

}
