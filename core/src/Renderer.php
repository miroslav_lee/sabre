<?php

namespace Sabre\Core;

use Sabre\Core\Service\Service;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Renderer of template.
 */
class Renderer {

  /**
   * Theme name.
   *
   * @var string
   */
  private $name;

  /**
   * Theme's variables.
   *
   * @var array
   */
  private $variables;

  /**
   * The Route Service.
   *
   * @var \Sabre\Core\Service\RouteService
   */
  private $routeService;

  /**
   * Renderer constructor.
   *
   * @param string $theme_name
   * @param array $variables
   *
   * @throws \Exception
   */
  public function __construct(string $theme_name, array $variables) {
    $this->name = $theme_name;
    $this->variables = $variables;
    $this->routeService = Service::get('route');
  }

  /**
   * Renders template.
   *
   * @return string
   *
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  public function render() {
    $loader = new FilesystemLoader($this->getTemplateDirectories());
    $twig = new Environment($loader, ['autoescape' => FALSE]);
    $route = $this->routeService->getCurrentRoute();
    $content = $twig->render($this->getFileName(), $this->variables);

    return $twig->render("html.html.twig", [
      'title' => $route->getTitle(),
      'menu' => isset($this->variables['menu']) ? $this->variables['menu'] : NULL,
      'content' => $content,
    ]);
  }

  /**
   * Gets template's file name.
   *
   * @return string
   */
  private function getFileName() {
    $name = str_replace('_', '-', $this->name);
    return "{$name}.html.twig";
  }

  /**
   * Gets template's file path.
   *
   * @return array
   */
  private function getTemplateDirectories(): array {
    return glob("./{core,modules\/*}/templates", GLOB_BRACE);
  }

}
