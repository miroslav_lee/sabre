<?php

namespace Sabre\Core\Controller;

interface ControllerInterface {

  /**
   * Return a page content.
   *
   * @return mixed
   */
  public function build();

}
