<?php

namespace Sabre\Core;

use Sabre\Core\Controller\ControllerInterface;

/**
 * Provide class for Route.
 */
class Route implements RouteInterface {

  /**
   * Route's name.
   *
   * @var string
   */
  private $name;

  /**
   * Route's path.
   *
   * @var string
   */
  private $path;

  /**
   * Page title.
   *
   * @var string
   */
  private $title;

  /**
   * The Controller object.
   *
   * @var \Sabre\Core\Controller\ControllerInterface
   */
  private $controller;

  /**
   * Path's arguments.
   *
   * @var array|false
   */
  private $arguments;

  /**
   * Path's components.
   *
   * @var array
   */
  private $pathComponents;

  /**
   * URI with origin components.
   *
   * @var string
   */
  private $uri;

  /**
   * Route's parameters.
   *
   * @var array
   */
  private $parameters;

  /**
   * Route constructor.
   *
   * @param string $name
   * @param string $path
   * @param \Sabre\Core\Controller\ControllerInterface $controller
   */
  public function __construct(string $name, string $path, ControllerInterface $controller) {
    $this->name = $name;
    $this->path = $path;
    $this->controller = $controller;

    // Init path's components.
    $path_components = $this->getPathComponents();
    $this->arguments = preg_grep('/{.*}/', $path_components);
  }

  /**
   * {@inheritDoc}
   */
  public function setTitle(string $title): RouteInterface {
    $this->title = $title;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setParameters(array $parameters): RouteInterface {
    $this->parameters = $parameters;

    if ($parameters) {
      $this->updateUri();
    }
    else {
      $this->setUri($this->getPath());
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setUri(string $uri): RouteInterface {
    $this->uri = $uri;
    $this->updateParameters();

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getUri(): string {
    return $this->uri;
  }

  /**
   * {@inheritDoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritDoc}
   */
  public function getPath(): string {
    return $this->path;
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * {@inheritDoc}
   */
  public function getController(): ControllerInterface {
    return $this->controller;
  }

  /**
   * {@inheritDoc}
   */
  public function getPathComponents(): array {
    if (!$this->pathComponents) {
      $this->pathComponents = explode('/', $this->getPath());
    }

    return $this->pathComponents;
  }

  /**
   * {@inheritDoc}
   */
  public function getArguments(): array {
    return $this->arguments;
  }

  /**
   * {@inheritDoc}
   */
  public function getParameter(string $name) {
    return $this->parameters[$name];
  }

  /**
   * Updates URI by Route's parameters.
   *
   * @return $this
   */
  protected function updateUri(): RouteInterface {
    foreach ($this->parameters as $name => $parameter) {
      $elements = $this->getPathComponents();
      $arguments = $this->getArguments();
      $index = array_search("{{$name}}", $arguments);
      $elements[$index] = $parameter;

      $this->setUri(implode('/', $elements));
    }

    return $this;
  }

  /**
   * Update Route's parameters by origin URI.
   *
   * @return $this
   */
  protected function updateParameters(): RouteInterface {
    $uri_components = explode('/', $this->getUri());
    $arguments = $this->getArguments();
    $parameters = [];

    foreach ($arguments as $index => $argument) {
      $value = $uri_components[$index];
      $name = preg_replace('/{*}*/', '', $argument);
      $parameters[$name] = $value;
    }

    $this->parameters = $parameters;

    return $this;
  }

}
