# Installation
- Install the Composer
- Run `composer install`
- Import `db.sql` from root directory
- Create the `settings.php` file in root directory
- Add data to Database connection. For example:
```
$database = [
  'name' => 'default',
  'user' => 'user',
  'password' => 'user',
  'host' => 'localhost:3600',
];
```
- Add the path to TMP directory.
```
$tmp_directory = '/tmp/';
```
