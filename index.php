<?php

use Sabre\Core\Service\Service;

require_once __DIR__ . '/vendor/autoload.php';

Service::init();
$route = Service::get('route');
$controller = $route->getCurrentRoute()->getController();
print $controller->build();
